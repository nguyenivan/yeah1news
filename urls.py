from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^MOReceiver', 'webservice01.views.vasc_service', name='vasc'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^y1n/acct/(?P<secret>\w*?)/$', 'webservice01.views.get_expiration', name='account')
)
from django.conf import settings
urlpatterns += patterns('', (
	r'^%s/(?P<path>.*)$' % settings.STATIC_URL.strip('/') ,
	'django.views.static.serve',
{'document_root': settings.STATIC_ROOT}
))



