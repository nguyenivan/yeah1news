from django.contrib import admin
from yeah1news.webservice01.models import ProductVersion, PhoneOperator

class ProductVersionAdmin(admin.ModelAdmin):
    pass
class PhoneOperatorAdmin(admin.ModelAdmin):
    pass
admin.site.register(ProductVersion, ProductVersionAdmin)
admin.site.register(PhoneOperator, PhoneOperatorAdmin)
