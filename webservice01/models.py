# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.fields.related import ForeignKey
from datetime import datetime, timedelta
from django.db import transaction
from django.conf import settings

class MOValidationException(Exception):
	pass
class ProductVersionException(Exception):
	pass

class MOErrorInterpreter(object):
	@classmethod
	def translate(cls,mt):
		errors = {
				'1' : u'Gửi thành công',
				'0' : u'Gửi tin nhắn bị lỗi',
				'-1' : u'Username hoặc Password không chính xác',
				'-2' : u'Nội dung tin nhắn không hợp lệ',
				'-3' : u'Số điện thoại sai',
				'-4' : u'Bị look tin nhắn (Cùng số điện thoại, nội dung và RequestID)',
				'-5' : u'Loại khác',
				'-6' : u'MessageType không hợp lệ', 
				'-7' : u'IsMore không chính xác',
				'-8' : u'Contenttype không chính xác',
				'-9' : u'RequestID không chính xác',
				'-10' : u'TotalMessage không chính xác',
				'-11' : u'CommandCode, ServiceID không chính xác',
				'-12' : u'MessageIndex không chính xác',
				'-15' : u'Server gửi tin có IP không chính xác',
				}
		message = errors.get(str(mt.Result))
		if message is None:
			return 'Error unknown'
		else:
			return message

class ProductVersion(models.Model):
	url = models.URLField()
	version = models.CharField(max_length = 20)
	is_current = models.BooleanField(_('Is Current'), default = False, blank = True)
	@transaction.commit_manually
	def set_current(self):
		try:
			others = ProductVersion.objects.exclude(pk=self.id)
			for other in others:
				other.is_current = False
				other.save()
			self.is_current = True
			self.save()
		except:
			transaction.rollback()
			raise
		else:
			transaction.commit()
	@classmethod
	def get_current(cls):
		from django.core.exceptions import MultipleObjectsReturned
		try:
			current = cls.objects.get(is_current = True)
		except (MultipleObjectsReturned, cls.DoesNotExist):
			raise ProductVersionException('Current version setting is not valid.')
		else:
			return current
			
			

class PhoneOperator(models.Model):
	name = models.CharField(_('Operator Name'), max_length=50)
	class Meta:
		verbose_name = _('Phone Operator')

class PhoneNumberPattern(models.Model):
	note = models.CharField(_('Note'), max_length = 50)
	number_pattern = models.CharField(_('Phone RegEx Pattern'), max_length=200)
	operator = models.ForeignKey(PhoneOperator)


class Account(models.Model):
	owner_name = models.CharField(_('Owner Name'), max_length = 100)
	balance = models.IntegerField(_('Current Balance'))
	expiration = models.DateTimeField(_('Expiration Date'))
	secret = models.CharField(_('Secret Key'), max_length = 50, unique = True)
	
	def calculate_expiration(self, service):
		if settings.SERVICE_DAYS[service] == 0:
			return self.expiration
		now = datetime.now()
		delta = timedelta( settings.SERVICE_DAYS[service] )
		if self.expiration is None or now>self.expiration:
			return now +  delta
		else:
			return self.expiration + delta
		#return  datetime.strptime('Jan 1 2012  1:33PM', '%b %d %Y %I:%M%p')
	
	def save(self, *args, **kwargs):
		super(Account,self).save(*args, **kwargs)
		
	@classmethod
	def normalize_phone(cls,phone):
		import re
		def a(phone):
			return phone
		def b(phone):
			return phone.lstrip('+')
		def c(phone):
			return '84' + phone.lstrip('0')

		pattern_methods = { 
							r'^849\d{8}$': a,
							r'^841\d{9}$': a,
							r'^\+849\d{8}$' :b,
							r'^\+841\d{9}$' :b,
							r'^09\d{8}$':c,
							r'^01\d{9}$':c,
							}
		for pattern in pattern_methods:
			if re.match(pattern, phone):
				result = pattern_methods[pattern](phone)
				return result
		raise MOValidationException('Phone number %s is not valid' %  (phone) )
	
	@classmethod
	@transaction.commit_manually
	def add_or_create(cls, number, service, secret):
		is_new = False
		try:
			phones = PhoneNumber.objects.filter(number = number).order_by('id')
			if phones:
				account = phones[0].account
				account.balance += settings.SERVICE_AMOUNTS[service]
				account.secret = secret
				account.expiration = account.calculate_expiration(service)
				account.save()
			else:
				try:
					account = Account.objects.get(secret=secret)
				except Account.DoesNotExist:
					is_new = True
					account = Account(balance = settings.SERVICE_AMOUNTS[service], secret = secret)
					account.expiration = account.calculate_expiration(service)
					account.save()
					phone = PhoneNumber(number = Account.normalize_phone(number), account = account)
					phone.save()
		except:
			transaction.rollback()
			raise
		else:
			transaction.commit()
			return (account, is_new)
				
class PhoneNumber(models.Model):
	number = models.CharField(_('Number in 849xxxx format'), max_length = 20, unique = True)
	operator = models.ForeignKey(PhoneOperator, blank = True, null = True) 
	account = models.ForeignKey(Account)

class Recharge(models.Model):
	amount = models.IntegerField(_('Amount'))
	datetime = models.DateTimeField(auto_now_add = True)
	account = models.ForeignKey(Account)
	
class Download(models.Model):
	version = models.ForeignKey(ProductVersion)
	datetime = models.DateTimeField(auto_now_add = True)
	account = models.ForeignKey(Account)



MESSAGE_FLOW_CHOICES = (
	('IC', _('Incoming')),
	('OG', _('Outgoing')),
)
MESSAGE_TYPE_CHOICES = (
	('RC', _('Recharge')),
	('DL', _('Download')),
	('GF', _('Gift')),
)

class Message(models.Model):
	flow = models.CharField(_('Flow'), choices = MESSAGE_FLOW_CHOICES, max_length = 2)
	content = models.CharField(_('Content'), max_length = 500)
	type = models.CharField(_('Type'), choices = MESSAGE_TYPE_CHOICES, max_length = 2)
	account = models.ForeignKey(Account)

class MO(models.Model):
	UserID = models.CharField(max_length = 20, blank = True)
	ServiceID = models.CharField(max_length = 10, blank = True)
	TotalMessage = models.CharField(max_length = 2, blank = True)
	Message = models.CharField(max_length = 500, blank = True)
	RequestID =  models.CharField(max_length = 10, blank = True)
	MoID = models.BigIntegerField(blank = True, null = True)
	Username = models.CharField(max_length = 50, blank = True)
	MessageIndex = models.CharField(max_length = 10, blank = True)
	IsMore = models.CharField(max_length = 10, blank = True)
	ContentType = models.CharField(max_length = 100, blank = True)
	CommandCode = models.CharField(max_length = 10, blank = True)
	MessageType = models.CharField(max_length = 1, blank = True)
	Result = models.CharField(max_length = 500, blank = True)
	
	def get_sub_commands(self):
		from exceptions import AttributeError 
		try:
			tokens = self.Message.split()
			if tokens and len(tokens)>1:
				tokens.pop(0)
				return tokens
			else:
				raise AttributeError
		except AttributeError:
			raise MOValidationException('Message is empty.')
			
	
