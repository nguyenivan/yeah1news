from celery.task import Task
from celery.registry import tasks
from webservice01.clients import VascClient
import copy

class TestTask(Task):
	def run(self, message, **kwargs):
		logger = self.get_logger(**kwargs)
		logger.info("Task runs: %s" % message)

class MTSender(Task):

	def run(self, mt, **kwargs):
		logger = self.get_logger(**kwargs)
		logger.info("Sendding message: %s" % mt.Message)
		VascClient.send_mt(mt)

class MOStore(Task):	 
	# direction = True for incoming, = False for outgoing
	def run(self, mo, direction = True, **kwargs): 
		logger = self.get_logger(**kwargs)
		mo.save()
		if direction:
			logger.info("Saved incoming message (id=%s): %s" % (mo.id, mo.Message) )
		else:
			logger.info("Saved outgoing message (id=%s): %s" % (mo.id, mo.Message) )

class MOError(Task):
	def run(self, mo, message, **kwargs):
		logger = self.get_logger(**kwargs)
		mt = copy.copy(mo)
		mt.MessageType = '1'
		mt.TotalMessage = '1'
		mt.MessageIndex = '1'
		mt.ContentType = '0'
		mt.IsMore = '0'
		mt.Message = 'Loi tin nhan: %s' %  message
		logger.info("Sendding error: %s" % mt.Message)
		VascClient.send_mt(mt)

tasks.register(TestTask)
tasks.register(MTSender)
tasks.register(MOStore)
