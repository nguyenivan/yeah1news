# soaplib v2.0.0beta2 (from memory)
# Django v1.3 (stable)
# NOTE: CSRF middleware has been turned off!
# For urls.py, see: https://gist.github.com/935812

import soaplib
from soaplib.core.service import rpc, DefinitionBase
from soaplib.core.model.primitive import String, Integer
from soaplib.core.model.clazz import Array
from clients import VascClient, MO_USERNAME, MO_PASSWORD
from django.views.decorators.csrf import csrf_exempt
from models import MO, MOValidationException, ProductVersionException, Account
from clients import CommandCodeException, ServiceIDException
import logging
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from webservice01.tasks import MTSender, MOStore, MOError

class VascService(DefinitionBase):

	@rpc(String,Integer,_returns=Array(String))
	def say_hello(self, name, times):
		results = []
		for i in range(0, times):
			results.append('Hello, %s' %name)
		return results
       
	@rpc(String,String,String,String,String,Integer,String,String,_returns=String)
	def messageReceiver(self, UserID,ServiceID,CommandCode,Message,RequestID,MoID,Username, Password):
		try:
			if Username != MO_USERNAME or Password != MO_PASSWORD:
				raise MOValidationException('Username/Password combination is not correct.')
			mo = MO( UserID = UserID,
					ServiceID = ServiceID,
					CommandCode = CommandCode,
					Message = Message,
					RequestID = RequestID,
					MoID = MoID,
					Username = Username 
					)
			client = VascClient(mo)
			processor = client.make_processor()
			mt = processor()
			MTSender.apply_async(args=[mt])
		except (MOValidationException, CommandCodeException, ServiceIDException), e:
			mo.Result = e.message
			MOError.apply_async(args=[mo, e.message])
		except Exception, e:
			import traceback
			mo.Result = traceback.format_exc(2)[:500]
			MOError.apply_async(args=[mo, 'Xin lien he nha cung cap Yeah1News.'])
		else:
			mo.Result = '1'
		finally:
			MOStore.apply_async(args=[mo,True])
			return '1'

from soaplib.core.server.wsgi import Application
from django.http import HttpResponse

import StringIO
class DumbStringIO(StringIO.StringIO):
    def read(self, n):
        return self.getvalue()

class DjangoVascApp(Application):
    def __call__(self, request):
        django_response = HttpResponse()
        def start_response(status, headers):
            status, reason = status.split(' ', 1)
            django_response.status_code = int(status)
            for header, value in headers:
                django_response[header] = value

        environ = request.META.copy()
        environ['CONTENT_LENGTH'] = len(request.raw_post_data)
        environ['wsgi.input'] = DumbStringIO(request.raw_post_data)
        environ['wsgi.multithread'] = False

        response = super(DjangoVascApp, self).__call__(environ, start_response)
        django_response.content = '\n'.join(response)
        return django_response

vasc_application = soaplib.core.Application([VascService], tns = 'http://www.silkcoast.com/soap/')

vasc_service = csrf_exempt(DjangoVascApp(vasc_application))

GET_EXPIRATION_CODE = (
	(0, 'Account expired'),
	(1, 'OK'),
	(-1, 'Invalid secret code'),
	(-2, 'Server error'),
)

from django.core.exceptions import MultipleObjectsReturned
from datetime import datetime
from django.http import HttpResponse
def get_expiration(request, secret):
	'Return: code, last charge, expiration date, number'
	try:
		try:
			account = Account.objects.get(secret = secret)
		except Account.DoesNotExist:
			result = '-1'
		else:
			expiration = account.expiration
			if expiration >= datetime.now():
				code = 1
			else:
				code = 0
			recharges = account.recharge_set.all().order_by('-datetime')
			if recharges:
				last_charge = recharges[0].amount
			else:
				last_charge = 0
				
			phones = account.phonenumber_set.all().order_by('id')
			if phones:
				number = phones[0].number
			else:
				number = '84000000000'
			#account.expiration.strftime('%d/%m/%Y')
			result = ','.join([str(code), str(last_charge), expiration.strftime('%d/%m/%Y'), number])
	except:
		result = '-2'
		# LOG BY EMAIL HERE #
		import traceback
		from django.core.mail import mail_admins
		mail_admins('Error in y1n.webservice01.get_expiration view ',  traceback.print_exc(), False)
	finally:
		return HttpResponse (result, mimetype = 'text/plain')

		






